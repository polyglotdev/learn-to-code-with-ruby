# frozen_string_literal: true

puts 'Hello world'
puts 'Hello, Dom'

puts 'I bought 5 dollars of shit at the store!'
puts 'I made $65 dollars or a profit of 25%'

puts 5
puts 3.14159

puts 4 + 3
puts '4' + '3'
puts
puts 'Dominique Hallan'

print 'Hello World'
print 'My name is Dom'
