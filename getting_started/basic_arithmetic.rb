# frozen_string_literal: true

p 1 + 4

# Subtraction
p 10 - 6

# Multiply
p 10 * 10

# Divide
p 10 / 5
p 12.0 / 5

# Exponents
p 2**3

# Modulo
p 5 % 2
p 14 % 4
