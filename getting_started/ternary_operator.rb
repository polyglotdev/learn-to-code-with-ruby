# frozen_string_literal: true

p 1 > 2 ? 'Yes, it is' : 'Nope, it aint'

def even_or_odd(number)
  p number.even? ? 'That number is even' : 'That number is odd'
end

even_or_odd(11)

# challenge
def pokey_challenge
  pokemon = 'Pikachu'
  p pokemon == 'Charizard' ? 'dumbass name' : "Ohhhhh I know who #{pokemon} is!"
end

pokey_challenge
