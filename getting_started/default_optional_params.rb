# frozen_string_literal: true

def giving_you_options(number, international_code=011, area_code=1)
  p "#{number} #{international_code} #{area_code}"
end

giving_you_options(3142237736)
