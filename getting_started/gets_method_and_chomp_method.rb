# Gets and Chomp Methods
=begin 
the gets method accept input from a user
chomp kills the new line \n when combined with gets
=end

p "Enter your name"
name = gets.chomp

p "Enter your age"
age = gets.chomp

p "#{name} is #{age} years old."

