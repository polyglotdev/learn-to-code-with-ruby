# frozen_string_literal: true

# This is a single line comment

# This
# is
# multiline
# comment!

# This is a multiline comment and not really preferred
# but you should use single octothorp

# Don't be the asshole that has comments all over the place!
# Comments can come before or after expressions check below
p 'This is valid evaluated Ruby Code' # This will not be printed

# I think this counts as a multi line comment
# rescue => exception
# but I dunno if my linter is going to hate it
# just for reference it wiped the real comment out. YAH linters...
