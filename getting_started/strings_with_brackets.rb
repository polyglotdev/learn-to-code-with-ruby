story = "Once upon a time in a land, far far away"
puts story.length
p story[3]
p story[-1]

p story.slice(3)

# Extact multiple characters from a string 
new_story = "A long time ago in a galaxy far, far away."
p new_story[8, 15]
p new_story[-1, 10]


p new_story.length
p new_story[0...20]


# Overwrite characters
thing = 'rocket ship'

thing[0] = 'po'
p thing