# frozen_string_literal: true

def password_checker(password)
  unless password.include?('a')
    puts 'It does not include the letter a'
  end
end

p password_checker('Apple')
