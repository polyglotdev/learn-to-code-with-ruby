# frozen_string_literal: true

def add(num1, num2)
  num1 + num2
end

def subtract(num1, num2)
  num1 - num2
end

def multiply(num1, num2)
  num1 * num2
end

def calc(number1, number2, operation = 'add')
  if operation == 'add'
    add(number1, number2)
  elsif operation == 'subtract'
    subtract(number1, number2)
  elsif operation == 'multiply'
    multiply(number1, number2)
  else
    'Where is the beef?'
  end
end

p calc(10, 4, 'add')
