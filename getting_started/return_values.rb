# frozen_string_literal: true

def add(num1, num2)
  p num1 + num2
end

add(2, 2)

def return_guess
  "The result of the add function is: #{add(3, 10)}"
end

return_guess

def huh
  'What will be the return value here?'
end

result = huh
p result
