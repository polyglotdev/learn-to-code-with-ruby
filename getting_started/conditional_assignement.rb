# frozen_string_literal: true

y = nil
p y

y ||= 5
p "#{y} is #{y}"

greeting = 'Hello'
extraction = 100
letter = greeting[extraction]
letter ||= "Not found because letter is #{letter.class}"
p letter
