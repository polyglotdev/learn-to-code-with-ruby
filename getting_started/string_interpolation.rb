name = 'Dom'

p "My name is #{name}, and I like Ruby"

p "#{name} is a software engineer that works at Gitlab"

# Old way
age = 25

p "I am " + age.to_s

p "I am #{age} years old."

p "The result of adding 1 + 3 is #{1 + 3}"

x = 5
y= 8

p "The sum of x and y is #{x + y}"