# Methods
p 'hello world'.length

# uppercase
p 'hello world'.upcase

# lowercase
p 'HELLO WORLD'.downcase

# variable declaration with method called on it
foo = 'hello world'
p foo.length

puts

# add one to number
p 10.next

# all p is puts method with the .inspect chained to it
