p 10.9.to_i.class
p 10.9.to_i

# round down
p 10.5.floor

# round up
p 10.5.ceil

# tradition mathematical rounding => .5 >= round up else round down. param will take points after decimal 
p 10.5.round(4)

# absolute value
checking = -12.abs
p checking

