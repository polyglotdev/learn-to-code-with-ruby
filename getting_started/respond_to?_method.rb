# frozen_string_literal: true

num = 1000
p num.respond_to?('class')
p num.respond_to?(:class)

def can_respond(num)
  if num.respond_to?(:next)
    p "#{num} CAN respond to the Next method"
  else
    p 'Not in my house'
  end
end

can_respond(2)
# A symbol is a string without the attached methods
