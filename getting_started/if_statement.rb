# frozen_string_literal: true

def it_bigger?(num1, num2)
  if num1 > num2
    p "#{num1} is greater than #{num2}"
  else
    p "#{num2} is greater than #{num1}"
  end
end

it_bigger?(199, 2)


def password_checker(password, user)
  if password === 'password'
    p "#{user}, your password is #{password}, change this bullshit"
  else
    p "Welcome, #{user}!"
  end
end

password_checker('fjdksalfj;asdf3838328479239423!', 'Dom')


def color_checker(color)
  if color == 'Red'
    p 'Color Match!!!'
  else
    p 'No match on color!'
  end
end

color_checker('Red')

def how
  name = 3
  if name == 2
    p 'Yasss'
  elsif 2 > 3
    p 'Noooo..'
  else
    p 'Loud Noises!!!!'
  end
end

how

def odd_or_even(number)
  if number.odd?
    p "#{number} is an odd number"
  else
    p "#{number} is even number."
  end
end

odd_or_even(23)
