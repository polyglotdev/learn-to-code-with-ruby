# frozen_string_literal: true

def fizz_buzz(number)
  i = 1
  until i > number
    if i % 5 == 0 && i % 3 == 0 
      puts "fizzbuzz -- #{i}"
    elsif i % 3 == 0 
      puts "fizz -- #{i}"
    elsif i % 5 == 0 
      puts "buzz -- #{i}"
    else
      puts i
    end
    i += 1
  end
end

p fizz_buzz(45)

def fizzed_out(number)
  i = 1
  for i in number do
    if (i % 5).zero? && (i % 3).zero?
      puts "#{i} - fizzbuzz"
    elsif (i % 3).zero?
      puts "#{i} - fizz"
    elsif (i % 5).zero?
      puts "#{i} - buzz"
    else
      puts i
    end
  end
end

p fizzed_out(45)
