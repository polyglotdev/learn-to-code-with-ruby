# frozen_string_literal: true

i = 1

while i < 10
  puts i
  i += 1
end

p i

def user_sign_in()
  status = true

  while status
    p 'Please enter username'
    username = gets.chomp.downcase
    p 'Please enter password'
    password = gets.chomp.downcase
    if username == 'dom' && password == 'dh1983'
      puts "Entry granted, #{username}"
      status = false
    else
      "Your username #{username} and or password #{password} is wrong, sucka."
      status = false
    end
  end
end

p user_sign_in
