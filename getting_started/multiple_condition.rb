# frozen_string_literal: true
def can_admit(age, ticket, id)
  if age >= 21 && ticket && id
    p 'Welcome to the Adult Shop.'
  else
    p 'You do not meet the requirements for entry.'
  end
end

can_admit(22, true, true)

def do_things(name, age)
  if name || age == 27
    p "Hey, #{name}"
  else
    p "Hey #{name}, you are too young to get in here."
  end
end

do_things('Dom', 27)
