# frozen_string_literal: true

def hey
  'hey'
end

result = hey
p result

def introduce_yourself
  puts 'My name is Dominique Hallan'
end

introduce_yourself_result = introduce_yourself
p introduce_yourself_result

def get_money(amount)
  name = 'Dominique'
  puts "#{name} made #{amount} last year in in salary"
end

amount_result = get_money(120_000_000.78)
p amount_result

def praise_person(name)
  puts "#{name} is amazing!"
  puts "#{name} is awesome!"
  puts "#{name} is awe inspiring!"
end

praise_person('Dominique Israel Hallan')

def add_two_numbers(num1, num2)
  p 'Ok solving your problem in the add two numbers method:'
  puts num1 + num2
end

add_two_numbers(10, 20)
add_two_numbers(10, 40)
