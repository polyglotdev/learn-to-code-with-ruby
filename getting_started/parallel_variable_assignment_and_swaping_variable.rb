# Variable Parallel Assignment
# a = 10
# b = 20
# c = 30

a, b, c = 10, 20, 30

p a, b, c

# Swap Variable Values
d = 1
e = 2

p d, e

d, e = e, d
p d, e
