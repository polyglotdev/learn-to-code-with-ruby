# frozen_string_literal: true

def rate_my_food(food)
  case food
  when 'Steak'
    'Pass the steak sauce!'
  when 'Sushi'
    'Hard freaking pass yo..'
  when 'Tacos', 'Burritos', 'Cheeseburger'
    'Yaaaas girl'
  else
    'Yeah I am not eating that.'
  end
end

p 'Dom'
p rate_my_food('Steak')
p rate_my_food('Tacos')

def grades(grade)
  case grade
  when 90..100 then 'A'
  when 80..89 then 'B'
  when 70..79 then 'C'
  when 60..69 then 'D'
  else
    'F'
  end
end

p grades(99)
p grades(89)
p grades(79)
p grades(69)
p grades(56)
