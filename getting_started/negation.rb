# frozen_string_literal: true

def get_username(name)
  if name.length != 3
    "hey, #{name}!"
  else
    'hey, Dom!'
  end
end

p get_username('Brooke')
