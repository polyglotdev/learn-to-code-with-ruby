# frozen_string_literal: true

def add(num1, num2)
  num1 + num2
end

def subtract(num1, num2)
  num1 - num2
end

def multiply(num1, num2)
  num1 * num2
end

def calc(num1, num2, operation = 'add')
  if operation == 'add'
    "The sum of #{num1} + #{num2} = #{add(num1, num2)}"
  elsif operation == 'subtract'
    "The sum of #{num1} - #{num2} = #{subtract(num1, num2)}"
  elsif operation == 'multiply'
    "The sum of #{num1} * #{num2} = #{multiply(num1, num2)}"
  end
end

add_result = calc(10, 2, 'add')
subtract_result = calc(10, 2, 'subtract')
multiply_result = calc(10, 2, 'multiply')

p add_result
p subtract_result
p multiply_result
