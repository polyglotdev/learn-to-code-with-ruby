# Ruby
## Getting Started
**`puts` method**
this method allows us to "print" text. Each `puts` method terminates with a line break.

**Example**:<br />
`puts 'Hello, Dom'`

- Also keep in ming that `puts` returns a `nil`!
- The `puts` method always adds the line break even if you don't give the method anything to print

**`print`** method<br>
Same as the `puts` method **EXCEPT** we don't get a line break after the statement<br>
**Example**:
```
print "Hello"
print "World"
```
This would return `HelloWorld` so as you can see, there is no line break in Hello to World even though they are on separate lines.

**`p`**<br>
`p` is yet another way that we can print something. Where `p` is different is that it gives us more feedback as to **what** was being printed.<br>
**Example**
```
puts 'Dom'
p 'Steven Seagal'
```
This gives us output of `Dom` and `"Dom"`. Do you see the difference? The second statement lets us know that indeed the output here is a string.
- We should also note that we get a line break here with `p` as well.
- Think of this as the literal print value when we use arrays, hashes and strings

**Basic Arithmetic**<br>
**add**:  `+` but this can also signify concatenation<br>
**subtract**:  `-` <br>
**multiply**:  `*` <br>
**divide**:  `/` **remember to add a zero for any floating point number between 0 and 1**<br>
**exponents**:  `**` (To the power of...) <br>
**modulo** `%` (Remainder Division)
- 🧠: When ruby expressions that are divide are calculated we do integer division which leads us to chop the decimal point off. The fix for this is to make one number or both numbers a floating point number <br>
- 🧠: Modulo took me a second to understand so I want to take the __math-y__ bullshit out of picture and just explain. If you were to evaluate the statement `5 % 2` The process to check, or let me rephrase that that, the process **that I used** was I took `2` and I see how many times `2` goes into `5` the correct answer is `2` times. _this is where the modulo portion comes in!_ So we have the result of `4` with a number `5` on the other side of that problem well because modulo seeks to get the remainder what is `5 - 4`? `1`!! So then the answer to `5 % 2` is `1` because there would be `1` left over. Modulo can also be used to check if a value is **even or odd** because if we have a variable and lets say its named even and we want to check if that variable is even we could take it `% 2` if the result of this expression is `0` then we have a even number because all even numbers are evenly divisible by the number 2.
